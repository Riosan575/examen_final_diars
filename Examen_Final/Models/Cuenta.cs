﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_Final.Models
{
    public class Cuenta
    {
        public int Id { get; set; }
        [Required (ErrorMessage = "El campo nombre es obligatorio")]
        public string Nombre { get; set; }
        [Required(ErrorMessage = "El campo tipo es obligatorio")]
        public string Tipo { get; set; }
        [Required(ErrorMessage = "El campo saldo es obligatorio")]
        public decimal Saldo { get; set; }
        public decimal Limite { get; set; }
        public int idUsuario { get; set; }
        public List<Gasto> gastos { get; set; }
        
    }
}
