﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_Final.Models
{
    public class Solicitud
    {
        public int Id { get; set; }
        public int IdUsuario { get; set; }
        public int IdAmigo { get; set; }
        public bool amistad { get; set; }
        public Account User { get; set; }
    }
}
