﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_Final.Models
{
    public class Gasto
    {
        public int Id { get; set; }
        public DateTime Fecha { get; set; }
        [Required(ErrorMessage = "El campo tipo es obligatorio")]
        public string Tipo { get; set; }
        [Required(ErrorMessage = "El campo descripción es obligatorio")]
        public string Descripcion { get; set; }
        [Required(ErrorMessage = "El campo monto es obligatorio")]
        public decimal Monto { get; set; }
        public int IdCuenta { get; set; }
    }
}
