﻿using Examen_Final.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_Final.DB.Mapping
{
    public class SolicitudMap : IEntityTypeConfiguration<Solicitud>
    {
        public void Configure(EntityTypeBuilder<Solicitud> builder)
        {
            builder.ToTable("Solicitud");
            builder.HasKey(o => o.Id);

            builder.HasOne(o => o.User)
                .WithMany()
                .HasForeignKey(o => o.IdAmigo);
        }
    }
}
