﻿using Examen_Final.DB;
using Examen_Final.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_Final.Controllers
{
    public class HomeController : Controller
    {
        private FinalContext context;
        public HomeController(FinalContext context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }
        [Authorize]
        [HttpGet]
        public IActionResult Usuarios()
        {
            var claim = HttpContext.User.Claims.First();
            string username = claim.Value;
            var user = context.Accounts.First(o => o.Usuario == username);
            var users = context.Accounts.Where(o => o.Id != user.Id).ToList();
            return View(users);
        }
        [Authorize]
        [HttpGet]
        public IActionResult Solicitudes()
        {
            var claim = HttpContext.User.Claims.First();
            string username = claim.Value;
            var user = context.Accounts.First(o => o.Usuario == username);
            var solicitudes = context.Solicitudes.
                Where(o => o.IdUsuario == user.Id && !o.amistad).
                Include(o => o.User).
                ToList();
            return View(solicitudes);
        }
        [Authorize]
        [HttpGet]
        public IActionResult Solicitud(Solicitud solicitud, int idUser)
        {
            var claim = HttpContext.User.Claims.First();
            string username = claim.Value;
            var user = context.Accounts.First(o => o.Usuario == username);
            solicitud.IdUsuario = user.Id;
            solicitud.IdAmigo = idUser;
            solicitud.amistad = false;
            var solicitud1 = new Solicitud
            {
                IdUsuario = idUser,
                IdAmigo = user.Id,
                amistad = false
            };

            var amigos = context.Solicitudes
                .Where(o => o.IdUsuario == user.Id && o.IdAmigo == idUser)
                .FirstOrDefault();

            if (amigos != null)
            {
                ModelState.AddModelError("Amigos", "Solicitud o amistad ya iniciada");
            }
            if (ModelState.IsValid)
            {
                context.Solicitudes.Add(solicitud);
                context.Solicitudes.Add(solicitud1);
                context.SaveChanges();
            }
            var users = context.Accounts.Where(o => o.Id != user.Id).ToList();
            return View("Usuarios", users);
        }
        [Authorize]
        [HttpGet]
        public IActionResult AceptaSolicitudes(int idUsuario)
        {
            var claim = HttpContext.User.Claims.First();
            string username = claim.Value;
            var user = context.Accounts.First(o => o.Usuario == username);
            var solitud1 = context.Solicitudes.Where(o => o.IdAmigo == idUsuario && o.IdUsuario == user.Id).First();
            var solitud2 = context.Solicitudes.Where(o => o.IdAmigo == user.Id && o.IdUsuario == idUsuario).First();

            solitud1.amistad = true;
            solitud2.amistad = true;
            context.SaveChanges();
            var solicitudes = context.Solicitudes.Where(o => o.IdUsuario == user.Id && !o.amistad).ToList();
            return View("Solicitudes", solicitudes);
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
