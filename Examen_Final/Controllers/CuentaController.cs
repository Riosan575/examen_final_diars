﻿using Examen_Final.DB;
using Examen_Final.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_Final.Controllers
{
    public class CuentaController : Controller
    {
        private FinalContext context;
        private IConfiguration configuration;
        public CuentaController(FinalContext context, IConfiguration configuration)
        {
            this.context = context;
            this.configuration = configuration;
        }
        [HttpGet]
        public IActionResult CrearCuenta()
        {
            return View();
        }
        [HttpGet]
        public IActionResult Transferencia()
        {
            ViewBag.cuentas = context.Cuentas.ToList();
            return View();
        }
        [HttpGet]
        public IActionResult Cuenta()
        {
            var claim = HttpContext.User.Claims.First();
            string username = claim.Value;
            var user = context.Accounts.First(o => o.Usuario == username);
            var cuentas = context.Cuentas.Where(o => o.idUsuario == user.Id);
            ViewBag.cuenta = context.Cuentas.ToList();
            return View(cuentas);
        }
        [HttpGet]
        public IActionResult Transacciones(int id)
        {
            ViewBag.transaccion = context.Gastos.Where(o => o.IdCuenta == id);
            ViewBag.cuenta = context.Cuentas.FirstOrDefault(o => o.Id == id);
            return View();
        }
        [HttpGet]
        public IActionResult CrearTransaccion(int id)
        {
            ViewBag.cuenta = id;
            return View();
        }
        [HttpPost]
        public IActionResult CrearTransaccion(Gasto gasto)
        {
            if (gasto.Tipo == "Gasto")
            {
                gasto.Fecha = DateTime.Now;
                var cuenta = context.Cuentas.Where(o => o.Id == gasto.IdCuenta).First();
                if (cuenta.Limite + cuenta.Saldo <= gasto.Monto)
                {
                    ModelState.AddModelError("Cuenta", "Monto superado");
                }
                if (ModelState.IsValid)
                {
                    gasto.Monto = gasto.Monto * -1;
                    context.Gastos.Add(gasto);
                    context.SaveChanges();
                    ModificaMontoCuenta(gasto.IdCuenta);
                }
                else
                {
                    ViewBag.cuenta = gasto.IdCuenta;
                    return View();
                }
            }
            if (gasto.Tipo == "Ingreso")
            {
                gasto.Fecha = DateTime.Now;
                context.Gastos.Add(gasto);
                context.SaveChanges();
                ModificaMontoCuenta(gasto.IdCuenta);
            }
            ViewBag.cuenta = gasto.IdCuenta;
            return RedirectToAction("Cuenta");
        }
        [HttpPost]
        public IActionResult CrearCuenta(Cuenta cuenta)
        {
            var claim = HttpContext.User.Claims.First();
            string username = claim.Value;
            var user = context.Accounts.First(o => o.Usuario == username);
            if (cuenta.Tipo == "Propio")
            {
                cuenta.idUsuario = user.Id;
                cuenta.Saldo = cuenta.Saldo;
                cuenta.Limite = 0;
                cuenta.gastos = new List<Gasto>
                {
                    new Gasto
                    {
                        Tipo = "Ingreso",
                        Fecha = DateTime.Now,
                        Monto = cuenta.Saldo,
                        Descripcion = "Primer monto"
                    }
                };
                context.Cuentas.Add(cuenta);
                context.SaveChanges();
            }
            if (cuenta.Tipo == "Crédito")
            {
                cuenta.idUsuario = user.Id;
                cuenta.Limite = cuenta.Saldo;
                cuenta.Saldo = 0;
                context.Cuentas.Add(cuenta);                
                context.SaveChanges();
            }
            
            return RedirectToAction("Cuenta");
        }
        [HttpPost]
        public IActionResult Transferencia(int cuentaOrigenId, int cuentaDestinoId, decimal monto)
        {
            var transaccion1 = new Gasto
            {
                IdCuenta = cuentaOrigenId,
                Fecha = DateTime.Now,
                Tipo = "Gasto",
                Descripcion = "Transferencia",
                Monto = monto * -1
            };

            var transaccion2 = new Gasto
            {
                IdCuenta = cuentaDestinoId,
                Fecha = DateTime.Now,
                Tipo = "Ingreso",
                Descripcion = "Transferencia",
                Monto = monto
            };

            context.Gastos.Add(transaccion1);
            context.Gastos.Add(transaccion2);

            context.SaveChanges();

            UpdateAmountAcount(cuentaOrigenId);
            UpdateAmountAcount(cuentaDestinoId);

            return RedirectToAction("Cuenta", "Cuenta");
        }
        private void UpdateAmountAcount(int accountId)
        {
            var account = context.Cuentas
                .Include("gastos")
                .FirstOrDefault(o => o.Id == accountId);

            var total = account.gastos.Sum(o => o.Monto);
            account.Saldo = total;
            context.SaveChanges();
        }
        private void ModificaMontoCuenta(int cuentaId)
        {
            var cuenta = context.Cuentas
                .Include("gastos")
                .FirstOrDefault(o => o.Id == cuentaId);

            var total = cuenta.gastos.Sum(o => o.Monto);
            cuenta.Saldo = total;
            context.SaveChanges();
        }
    }
}
