﻿using Examen_Final.DB;
using Examen_Final.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Examen_Final.Controllers
{
    public class LoginController : Controller
    {
        private FinalContext context;
        private IConfiguration configuration;
        public LoginController(FinalContext context, IConfiguration configuration)
        {
            this.context = context;
            this.configuration = configuration;
        }
        public IActionResult Login()
        {
            return View();
        }
        public IActionResult Register()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Login(string username, string password)
        {
            var user = context.Accounts
                .FirstOrDefault(o => o.Usuario == username && o.Contraseña == CreateHash(password));

            if (user == null)
            {
                TempData["AuthMessage"] = "Usuario o Contraseña incorrecto";
                return RedirectToAction("Login");
            }

            // Autenticar
            var claims = new List<Claim> {
                new Claim(ClaimTypes.Name, user.Usuario),
            };

            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);

            HttpContext.SignInAsync(claimsPrincipal);


            return RedirectToAction("Index", "Home");
        }
        [HttpPost]
        public ActionResult Register(Account account) // POST
        {
            if (ModelState.IsValid)
            {
                account.Contraseña = CreateHash(account.Contraseña);
                context.Accounts.Add(account);
                context.SaveChanges();
                return RedirectToAction("Login");
            }
            return View("Register", account);
        }
        [HttpGet]
        public IActionResult Logout()
        {
            HttpContext.SignOutAsync();
            return RedirectToAction("Login");
        }
        private string CreateHash(string input)
        {
            var sha = SHA512.Create();
            input += configuration.GetValue<string>("Key");
            var hash = sha.ComputeHash(Encoding.Default.GetBytes(input));

            return Convert.ToBase64String(hash);
        }
    }
}
